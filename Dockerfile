FROM node:14-alpine

COPY client ./client
COPY config ./config
COPY middlewares ./middlewares
COPY models ./models
COPY repositories ./repositories
COPY routes ./routes
COPY services ./services

COPY build-start.sh ./
COPY index.js ./

COPY package*.json ./

RUN ./build-start.sh

EXPOSE 3050
CMD [ "node", "index.js" ]


