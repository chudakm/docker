class NotFoundError extends Error {
  constructor(message) {
    super(message);
  }
}

exports.NotFoundError = NotFoundError;
