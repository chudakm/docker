class ValidationError extends Error {}

const defaultOptions = { required: true, minLength: 3, maxLength: 30 };
const defaultGmailOptions = { ...defaultOptions, minLength: 10 };

class Validator {
  constructor(body) {
    this.body = body;
  }

  static validate(body) {
    return new Validator(body);
  }

  haveOnlyAllRequiredFields(...requiredFields) {
    const bodyFields = Object.keys(this.body);
    const haveOnlyRequired = bodyFields.every(field => requiredFields.includes(field));
    const haveAllRequired = requiredFields.every(field => bodyFields.includes(field));

    if (!haveOnlyRequired) throw new ValidationError(`body should contain only [${requiredFields}] fields`);
    if (!haveAllRequired) throw new ValidationError(`body should contain [${requiredFields}] fields`);

    return this;
  }

  haveOnlyAtLeastOneRequiredField(...requiredFields) {
    const bodyFields = Object.keys(this.body);
    const haveOnlyRequired = bodyFields.every(field => requiredFields.includes(field));
    const haveAtLeastOneRequired = bodyFields.length > 0;

    if (!haveOnlyRequired) throw new ValidationError(`body should contain only [${requiredFields}]`);
    if (!haveAtLeastOneRequired) throw new ValidationError(`body should contain at least one of this fields [${requiredFields}]`);

    return this;
  }

  isFieldString(field, options) {
    options = { ...defaultOptions, ...options };

    const value = this.body[field];
    if (value === "") throw new ValidationError(`${field} cannot be empty`);

    const trimmedValue = value && typeof value === "string" && value.trim();
    if (!trimmedValue && !options.required) return this;

    this.body[field] = trimmedValue;

    if (trimmedValue && trimmedValue.length >= options.minLength && trimmedValue.length < options.maxLength) return this;
    else throw new ValidationError(`${field} should be longer than ${options.minLength} and shorter than ${options.maxLength}`);
  }

  isFieldGmail(field, options) {
    options = { ...defaultGmailOptions, ...options };

    const value = this.body[field];
    if (!value && !options.required) return this;

    if (
      value &&
      typeof value === "string" &&
      value.endsWith("@gmail.com") &&
      value.length > options?.minLength &&
      value.length < options?.maxLength
    )
      return this;
    else throw new ValidationError(`${field} should be gmail`);
  }

  isFieldUkrainianPhoneNumber(field, options = { required: false }) {
    const value = this.body[field];
    if (!value && !options.required) return this;

    const numbersAndPlus = ["+", ...Array.from({ length: 10 }, (_, i) => String(i))];

    if (
      value &&
      typeof value === "string" &&
      value.startsWith("+380") &&
      this.isFieldContainsOnlyAllowedSymbols(field, numbersAndPlus) &&
      value.length === 13
    )
      return this;
    else throw new ValidationError(`${field} should be phone number in format +380xxx and length = 13`);
  }

  isFieldContainsOnlyAllowedSymbols(field, allowedSymbols = []) {
    const value = this.body[field];

    if (value && typeof value === "string" && value.split("").every(symbol => allowedSymbols.includes(symbol))) return this;
    else throw new ValidationError(`${field} should contain only [${allowedSymbols}]`);
  }

  isFieldInRange(field, min, max, options = { required: true }) {
    const value = this.body[field];
    if (!value && !options.required) return this;

    if (value && typeof value === "number" && value > min && value < max) return this;
    else throw new ValidationError(`${field} should be in range [${min}, ${max}]`);
  }
}

exports.Validator = Validator;
exports.ValidationError = ValidationError;
