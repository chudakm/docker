const { NotFoundError } = require("../config/not-found");
const { ValidationError } = require("../config/validator");

const responseMiddleware = (req, res, next) => {
  if (res.err) {
    const { err } = res;
    const response = { error: true, message: err.message };

    if (err instanceof ValidationError) return res.status(400).send(response);
    if (err instanceof NotFoundError) return res.status(404).send(response);

    res.status(400).send(response);
  } else {
    res.status(200).send(res.body);
  }
};

exports.responseMiddleware = responseMiddleware;
