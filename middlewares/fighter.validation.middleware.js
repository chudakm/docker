const { fighter } = require("../models/fighter");
const { Validator } = require("../config/validator");

const createFighterValid = (req, res, next) => {
  try {
    const requiredFields = Object.keys(fighter).filter(key => key !== "id" && key !== "health");
    Validator.validate(req.body)
      .haveOnlyAllRequiredFields(...requiredFields)
      .isFieldString("name")
      .isFieldInRange("power", 1, 100)
      .isFieldInRange("defense", 1, 10);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  try {
    const requiredFields = Object.keys(fighter).filter(key => key !== "id");
    Validator.validate(req.body)
      .haveOnlyAtLeastOneRequiredField(...requiredFields)
      .isFieldString("name")
      .isFieldInRange("health", 80, 120, { required: false })
      .isFieldInRange("power", 1, 100, { required: false })
      .isFieldInRange("defense", 1, 10, { required: false });
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
