const { user } = require("../models/user");
const { Validator } = require("../config/validator");

const createUserValid = (req, res, next) => {
  try {
    const requiredFields = Object.keys(user).filter(key => key !== "id");
    Validator.validate(req.body)
      .haveOnlyAllRequiredFields(...requiredFields)
      .isFieldString("firstName")
      .isFieldString("lastName")
      .isFieldGmail("email")
      .isFieldUkrainianPhoneNumber("phoneNumber")
      .isFieldString("password");
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const requiredFields = Object.keys(user).filter(key => key !== "id");
    Validator.validate(req.body)
      .haveOnlyAtLeastOneRequiredField(...requiredFields)
      .isFieldString("firstName", { required: false })
      .isFieldString("lastName", { required: false })
      .isFieldGmail("email", { required: false })
      .isFieldUkrainianPhoneNumber("phoneNumber", { required: false })
      .isFieldString("password", { required: false });
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
