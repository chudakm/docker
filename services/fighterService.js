const { FighterRepository } = require("../repositories/fighterRepository");
const { NotFoundError } = require("../config/not-found");

class FighterService {
  getAll() {
    return FighterRepository.getAll();
  }

  getById(id) {
    const fighter = FighterRepository.getOne({ id });
    if (!fighter) throw new NotFoundError(`Fighter with id ${id} not found.`);

    return fighter;
  }

  create(entity) {
    const isNotUniqueName = FighterRepository.searchOne("name", entity.name);

    if (isNotUniqueName) throw new Error("Fighter name should be unique");

    return FighterRepository.create({ ...entity, health: 100 });
  }

  updateById(id, entity) {
    const fighter = FighterRepository.getOne({ id });
    if (!fighter) throw new NotFoundError(`Fighter with id ${id} not found.`);

    return FighterRepository.update(id, entity);
  }

  deleteById(id) {
    const fighter = FighterRepository.getOne({ id });
    if (!fighter) throw new NotFoundError(`Fighter with id ${id} not found.`);

    return FighterRepository.delete(id);
  }
}

module.exports = new FighterService();
