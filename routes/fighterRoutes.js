const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const { createFighterValid, updateFighterValid } = require("../middlewares/fighter.validation.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    const fighters = FighterService.getAll();
    res.body = fighters;

    next();
  },
  responseMiddleware,
);

router.get(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;

    try {
      const fighter = FighterService.getById(id);
      res.body = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.post(
  "/",
  createFighterValid,
  (req, res, next) => {
    if (res.err) return next();

    try {
      const fighter = FighterService.create(req.body);
      res.body = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.put(
  "/:id",
  updateFighterValid,
  (req, res, next) => {
    if (res.err) return next();

    const { id } = req.params;

    try {
      const fighter = FighterService.updateById(id, req.body);
      res.body = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.delete(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;

    try {
      const fighter = FighterService.deleteById(id);
      res.body = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

module.exports = router;
