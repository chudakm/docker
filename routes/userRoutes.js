const { Router } = require("express");
const UserService = require("../services/userService");
const { createUserValid, updateUserValid } = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    const users = UserService.getAll();
    res.body = users;

    next();
  },
  responseMiddleware,
);

router.get(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;

    try {
      const user = UserService.getById(id);
      res.body = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.post(
  "/",
  createUserValid,
  (req, res, next) => {
    if (res.err) return next();

    try {
      const user = UserService.create(req.body);
      res.body = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.put(
  "/:id",
  updateUserValid,
  (req, res, next) => {
    if (res.err) return next();

    const { id } = req.params;

    try {
      const user = UserService.updateById(id, req.body);
      res.body = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.delete(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;

    try {
      const user = UserService.deleteById(id);
      res.body = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

module.exports = router;
