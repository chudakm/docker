const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const { loginValid } = require("../middlewares/auth.validation.middleware");

const router = Router();

router.post(
  "/login",
  loginValid,
  (req, res, next) => {
    try {
      console.log(req.body);
      const user = AuthService.login(req.body);
      res.body = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

module.exports = router;
